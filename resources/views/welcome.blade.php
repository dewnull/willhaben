<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>The Stuff</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <style>
        :root {
            --jumbotron-padding-y: 3rem;
        }

        .jumbotron {
            padding-top: var(--jumbotron-padding-y);
            padding-bottom: var(--jumbotron-padding-y);
            margin-bottom: 0;
            background-color: #fff;
        }
        @media (min-width: 768px) {
            .jumbotron {
                padding-top: calc(var(--jumbotron-padding-y) * 2);
                padding-bottom: calc(var(--jumbotron-padding-y) * 2);
            }
        }

        .jumbotron p:last-child {
            margin-bottom: 0;
        }

        .jumbotron-heading {
            font-weight: 300;
        }

        .jumbotron .container {
            max-width: 40rem;
        }

        footer {
            padding-top: 3rem;
            padding-bottom: 3rem;
        }

        footer p {
            margin-bottom: .25rem;
        }

        .box-shadow { box-shadow: 0 .25rem .75rem rgba(0, 0, 0, .05); }
    </style>
</head>

<body>

<header>
    <div class="collapse bg-dark" id="navbarHeader">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-md-7 py-4">
                    <h4 class="text-white">Boo!</h4>
                </div>
            </div>
        </div>
    </div>
    <div class="navbar navbar-dark bg-dark box-shadow">
        <div class="container d-flex justify-content-between">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarHeader" aria-controls="navbarHeader" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
        </div>
    </div>
</header>

    <main role="main">

        <section class="jumbotron text-center">
            <div class="container">
                <h1 class="jumbotron-heading">Find the flat!</h1>
                <p class="lead text-muted"> Now or never!</p>
            </div>
        </section>

        <div class="album py-5 bg-light">
            <div class="container">
                    @if(count($new))
                        <h3 class="text">NEW</h3>
                        @foreach([7,8,5,2,3,4,6] as $number)

                            <div class="row">
                                @foreach($new->where('berzik', $number) as $item)

                                    <div class="col-md-4">
                                        <div class="card mb-4 box-shadow">
                                            <div class="card-body">
                                                <h3>#{{ $number }}</h3>
                                                <a href="{{ $item->link }}">
                                                    <img src="{!! $item->img !!}" width="130px" height="130px">
                                                </a>
                                            </div>
                                            <div class="card-body">

                                                <div class="d-flex justify-content-between align-items-center">
                                                    <p class="text-muted">{!! $item->title !!}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                @endforeach
                            </div>
                            <hr>
                        @endforeach
                    @endif

                    @if(count($old))
                        <div class="container bg-warning">
                            @foreach([7,8,5,2,3,4,6] as $number)

                                <div class="row">
                                    @foreach($old->where('berzik', $number) as $item)

                                        <div class="col-md-4">
                                            <div class="card mb-4 box-shadow">
                                                <div class="card-body">
                                                    <h3>#{{ $number }}</h3>
                                                    <a href="{{ $item->link }}">
                                                        <img src="{!! $item->img !!}" width="130px" height="130px">
                                                    </a>
                                                </div>
                                                <div class="card-body">

                                                    <div class="d-flex justify-content-between align-items-center">
                                                        <p class="text-muted">{!! $item->title !!}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    @endforeach
                                </div>
                                <hr>
                            @endforeach
                        </div>
                    @endif
            </div>
        </div>

    </main>

    <footer class="text-muted">
        <div class="container">
            <p class="float-right">
                <a href="#">Back to top</a>
            </p>
            <p>Album example is &copy; Bootstrap, but please download and customize it for yourself!</p>
        </div>
    </footer>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
</body>
</html>
