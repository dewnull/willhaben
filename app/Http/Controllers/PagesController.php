<?php

namespace App\Http\Controllers;

use App\Models\Item;
use Carbon\Carbon;

class PagesController extends Controller
{
    public function main()
    {
        $dayAgo = Carbon::now()->subDay();
        $items = Item::where('created_at', '>=', $dayAgo->toDateString())->orderBy('created_at')->get();
        $new = $items->where('created_at', '>=', Carbon::now()->toDateString());
        $old = $items->where('created_at', '<', Carbon::now()->toDateString());

        return view('welcome', compact('new', 'old'));
    }
}
