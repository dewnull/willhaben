<?php

declare(strict_types=1);

namespace App\Console\Commands\DataScrapper;

use App\Console\Commands\DataScrapper\Berzike\EightBerzik;
use App\Console\Commands\DataScrapper\Berzike\FifthBerzik;
use App\Console\Commands\DataScrapper\Berzike\FourthBerzik;
use App\Console\Commands\DataScrapper\Berzike\SecondBerzik;
use App\Console\Commands\DataScrapper\Berzike\SevenBerzik;
use App\Console\Commands\DataScrapper\Berzike\SixthBerzik;
use App\Console\Commands\DataScrapper\Berzike\ThirdBerzik;
use Illuminate\Console\Command;

class DataScrapperCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'items:scrap';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    protected array $berzirke = [
        SevenBerzik::class,
        EightBerzik::class,
        FifthBerzik::class,
        SecondBerzik::class,
        ThirdBerzik::class,
        FourthBerzik::class,
        SixthBerzik::class
    ];

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        foreach ($this->berzirke as $berzirk) {
            app($berzirk)->scrapItems()->insertIfNotExist();
        }

        return 0;
    }
}
