<?php

declare(strict_types=1);

namespace App\Console\Commands\DataScrapper\Contracts;

interface HasNumber
{
    public function getNumber(): int;
}
