<?php

declare(strict_types=1);

namespace App\Console\Commands\DataScrapper\Berzike;

use App\Console\Commands\DataScrapper\Contracts\HasLink;
use App\Console\Commands\DataScrapper\Contracts\HasNumber;
use App\Models\Item;
use Carbon\Carbon;
use PHPHtmlParser\Dom;

abstract class Base implements HasLink, HasNumber
{
    protected array $newItems = [];
    protected array $fetchedItems = [];

    public function scrapItems(): self
    {
        $dom = new Dom;
        $dom->loadFromUrl(static::getLink());
        $contents = $dom->find('.search-result-entry');

        foreach ($contents as $content) {
            $section = $content->find('.content-section');
            $imageSection = $content->find('.image-section');
            $link = '';
            $image = '';
            $title = '';

            if ($content->find('.header.w-brk')->count() &&
                $content->find('.header.w-brk')->find('a')->count()
            ) {
                $link = 'https://www.willhaben.at' . $content->find('.header.w-brk')->find('a')->getAttribute('href');
            }

            if ($imageSection->count() && $imageSection->find('img')->count()) {
                $image = $imageSection->find('img')->getAttribute('src');
            }

            if ($content->find('.header.w-brk span')->count() &&
                $content->find('.header.w-brk span')->text
            ) {
                $title = \trim($content->find('.header.w-brk span')->text);
            }

            if ($section->count() && $section->find('span')->outerHtml) {
                $outerHtml = \mb_strtolower($section->find('span')->outerHtml);
                if (\strpos($outerHtml, 'provisionsfrei') !== false) {
                    $this->fetchedItems[] = [
                        'link' => $link,
                        'img' => $image,
                        'title' => $title
                    ];
                    continue;
                }
            }

            if ($section->count() && $section->find('.bottom-content')->count()) {
                $bottomContent = \mb_strtolower($section->find('.bottom-content')->outerHtml);
                if (\strpos($bottomContent, 'privat') !== false) {
                    $this->fetchedItems[] = [
                        'link' => $link,
                        'img' => $image,
                        'title' => $title,
                    ];
                }
            }
        }

        return $this;
    }

    public function insertIfNotExist(): self
    {
        $newItems = [];

        foreach ($this->fetchedItems as $item) {
            $record = Item::where('link', $item['link'])->first();

            if (!$record) {
                $newItems[] = [
                    'link' => $item['link'],
                    'created_at' => Carbon::now(),
                    'berzik' => static::getNumber(),
                    'img' => $item['img'],
                    'title' => $item['title'],
                ];
            }
        }

        if ($newItems) {
            $this->newItems = $newItems;
            Item::insert($newItems);
        }

        return $this;
    }
}
