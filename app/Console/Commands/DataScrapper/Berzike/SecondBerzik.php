<?php

declare(strict_types=1);

namespace App\Console\Commands\DataScrapper\Berzike;

class SecondBerzik extends Base
{
    public function getLink(): string
    {
        return 'https://www.willhaben.at/iad/immobilien/mietwohnungen/mietwohnung-angebote?&PRICE_TO=700&areaId=117224&parent_areaid=900';
    }

    public function getNumber(): int
    {
        return 2;
    }
}
