<?php

declare(strict_types=1);

namespace App\Console\Commands\DataScrapper\Berzike;

class ThirdBerzik extends Base
{
    public function getLink(): string
    {
        return 'https://www.willhaben.at/iad/immobilien/mietwohnungen/mietwohnung-angebote?&PRICE_TO=700&areaId=117225&parent_areaid=900';
    }

    public function getNumber(): int
    {
        return 3;
    }
}
